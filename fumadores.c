#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <pthread.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

#define MAX_BUFFER 150
#define DATOS_A_PRODUCIR 100
#define clear() printf("\033[H\033[J")



sem_t agente;
sem_t cerillos;
sem_t papel;
sem_t tabaco;
int iteracion=0;

pthread_mutex_t mutex=PTHREAD_MUTEX_INITIALIZER;
void *Agente (void *);
void *Papel (void *);
void *Tabaco (void *);
void *Cerillos (void *);
const char * Ingrediente(int i);


void main (void){
  pthread_t th1,th2,th3,th4,th5;
    sem_init (&agente, 0, 0);
      sem_init (&cerillos, 0, 0);
        sem_init (&papel, 0, 0);
          sem_init (&tabaco, 0, 0);

            pthread_create (&th1, NULL, Agente, NULL);
              pthread_create (&th2, NULL, Papel, NULL);
                pthread_create (&th3, NULL, Tabaco, NULL);
                  pthread_create (&th4, NULL, Cerillos, NULL);

                    pthread_join(th1, NULL);
                      pthread_join(th2, NULL);
                        pthread_join(th3, NULL);
                          pthread_join(th4, NULL);

                            sem_destroy(&agente);
                              sem_destroy(&cerillos);
                                sem_destroy(&papel);
                                  sem_destroy(&tabaco);
                                    exit(0);
                                    }

                                    void *Agente (void *n)
                                    {
                                      int ing1,ing2;
                                      	time_t t;
                                      		srand((unsigned) time(&t));
                                      		  while(1){
                                      		  		do{
                                      		  					ing1=rand()%3;
                                      		  								ing2=rand()%3;
                                      		  										}while(ing1==ing2);
                                      		  												iteracion++;
                                      		  														printf("Iteración No.:%i\n",iteracion);
                                      		  																printf("Ingrediente 1: %s\n",Ingrediente(ing1));
                                      		  																	  printf("Ingrediente 2: %s\n",Ingrediente(ing2));
                                      		  																	  		if((ing1==1 && ing2==2) || (ing1==2 && ing2==1)){
                                      		  																	  					sem_post(&papel);
                                      		  																	  								sem_wait(&agente);
                                      		  																	  										}else if((ing1==0 && ing2==1) || (ing1==1 && ing2==0)){
                                      		  																	  													sem_post(&cerillos);
                                      		  																	  																sem_wait(&agente);
                                      		  																	  																		}else if((ing1==0 && ing2==2) || (ing1==2 && ing2==0)){
                                      		  																	  																					sem_post(&tabaco);
                                      		  																	  																								sem_wait(&agente);
                                      		  																	  																										}else{
                                      		  																	  																													printf("Error");
                                      		  																	  																															}
//                                      		  																	  																																	sleep(2);
//                                      		  																	  																																	    clear();
                                      		  																	  																																	      }
                                    		  																	  																																	        pthread_exit(0);
                                    		  																	  																																	        };
                                      		  																	  																																	        void *Papel (void *j)
                                      		  																	  																																	        {
                                      		  																	  																																	          while(1){
                                      		  																	  																																	          		sem_wait(&papel);
                                      		  																	  																																	          				printf("Papel fumando**\n");
                                      		  																	  																																	          						sleep(5);
                                      		  																	  																																	          								sem_post(&agente);
                                      		  																	  																																	          									}
                                      		  																	  																																	          									  pthread_exit(0);
                                      		  																	  																																	          									  };

                                      		  																	  																																	          									  void *Tabaco (void *j)
                                      		  																	  																																	          									  {
                                      		  																	  																																	          									    while(1){
                                      		  																	  																																	          									    		sem_wait(&tabaco);
                                      		  																	  																																	          									    				printf("Tabaco fumando**\n");
                                      		  																	  																																	          									    						sleep(5);
                                      		  																	  																																	          									    								sem_post(&agente);
                                      		  																	  																																	          									    									}
                                      		  																	  																																	          									    									  pthread_exit(0);
                                      		  																	  																																	          									    									  };

                                      		  																	  																																	          									    									  void *Cerillos (void *j)
                                      		  																	  																																	          									    									  {
                                      		  																	  																																	          									    									    while(1){
                                      		  																	  																																	          									    									    		sem_wait(&cerillos);
                                      		  																	  																																	          									    									    				printf("Cerillos fumando**\n");
                                      		  																	  																																	          									    									    						sleep(5);
                                      		  																	  																																	          									    									    								sem_post(&agente);
                                      		  																	  																																	          									    									    									}
                                      		  																	  																																	          									    									    									  pthread_exit(0);
                                      		  																	  																																	          									    									    									  }
				 const char * Ingrediente(int i){
                                		  																	  																																	          									    									    									  	switch(i){
                                      		  																	  																																	          									    									    									  			case 0:
                                      		  																	  																																	          									    									    									  						return "Papel";
                                      		  																	  																																	          									    									    									  								break;
                                      		  																	  																																	          									    									    									  										case 1:
                                      		  																	  																																	          									    									    									  													return "Tabaco";
                                      		  																	  																																	          									    									    									  															break;
                                      		  																	  																																	          									    									    									  																	case 2:
                                      		  																	  																																	          									    									    									  																				return "Cerillo";
                                      		  																	  																																	          									    									    									  																						break;
                                      		  																	  																																	          									    									    									  																							}
                              		  																	  																																	          									    									    									  																							}
