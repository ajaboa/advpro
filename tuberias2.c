/*Tuberías con nombre*/
#include <sys/types.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

#define MAX 256

void main(void){
int pid, *estado;
char pipe1_nombre[MAX]; //Identificador de la tuberia 1
char mensaje[MAX]; // Mensaje a escribir o leer
int fifo1;
int fifo2;

//Crea las tuberías
mknod(pipe1_nombre, S_IFIFO |0666, 0);
if((pid=fork())==0)/*CÓDIGO DEL HIJO 1*/{
	fifo1 = open(pipe1_nombre,O_WRONLY);
	strcpy(mensaje,"Texto");
	printf("Soy el hijo 1 el mensaje a enviar: %s \n", mensaje);
	write(fifo1,mensaje, strlen(mensaje)+1);
	close(fifo1);
	exit(0);
}//FIN DEL CODIGO HIJO 1
if((pid=fork())==0)/*CÓDIGO DEL HIJO 2*/{
	fifo1=open(pipe1_nombre, O_RDONLY);
	read(fifo1, mensaje, MAX);
	printf("Soy el hijo 2 y recibo el mensaje %s\n",mensaje);
	close(fifo1);
	exit(0);
}/*FIN DEL HIJO 2*/

else{/*CÓDIGO DEL PADRE*/
exit(0);
}
}
